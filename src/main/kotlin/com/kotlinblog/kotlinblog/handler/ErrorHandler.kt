package com.kotlinblog.kotlinblog.handler

import com.kotlinblog.kotlinblog.error.ErrorMessage
import com.kotlinblog.kotlinblog.exception.UserNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ErrorHandler {

    @ExceptionHandler(UserNotFoundException::class)
    fun userNotFoundException(exception: UserNotFoundException) = ResponseEntity(ErrorMessage(message = exception.message),HttpStatus.NOT_FOUND)

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun methodArgumentNotValidException(exception: MethodArgumentNotValidException) =
        ResponseEntity(exception.fieldError?.defaultMessage?.let { ErrorMessage(message = it) },HttpStatus.BAD_REQUEST)
}