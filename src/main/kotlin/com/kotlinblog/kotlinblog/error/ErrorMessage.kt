package com.kotlinblog.kotlinblog.error

import java.time.LocalDateTime

data class ErrorMessage(
    val timestamp: LocalDateTime = LocalDateTime.now(),
    val message: String
)
