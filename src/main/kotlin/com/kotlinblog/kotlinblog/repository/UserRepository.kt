package com.kotlinblog.kotlinblog.repository

import com.kotlinblog.kotlinblog.model.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface UserRepository: CrudRepository<User,UUID>{
}