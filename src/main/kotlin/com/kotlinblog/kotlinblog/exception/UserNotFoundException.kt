package com.kotlinblog.kotlinblog.exception

class UserNotFoundException(override val message: String = "user not found"): RuntimeException(message) {
}
