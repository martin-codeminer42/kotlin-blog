package com.kotlinblog.kotlinblog.model

import com.kotlinblog.kotlinblog.dto.UserDto
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class User(
    @Id
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val email: String,
    val password: String
) {
    fun toUserDto() = UserDto(id = id, name = name, email = email)
}