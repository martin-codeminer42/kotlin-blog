package com.kotlinblog.kotlinblog.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.kotlinblog.kotlinblog.model.User
import java.util.UUID
import javax.validation.constraints.NotBlank

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class UserDto(
    val id: UUID? = null,
    @field:NotBlank(message = "nome não pode ser nulo ou vazio")
    val name: String,
    @field:NotBlank
    val email: String,
    @field:NotBlank
    val password: String = ""
) {
    fun toUser() = User(name = name, email = email, password = password)
}
