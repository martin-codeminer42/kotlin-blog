package com.kotlinblog.kotlinblog.dto

data class UpdateUserDto(
    val name: String?,
    val email: String?,
    val password: String?
)
