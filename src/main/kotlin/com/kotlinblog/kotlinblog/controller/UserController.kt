package com.kotlinblog.kotlinblog.controller

import com.kotlinblog.kotlinblog.dto.UpdateUserDto
import com.kotlinblog.kotlinblog.dto.UserDto
import com.kotlinblog.kotlinblog.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.util.UUID
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UserController(
    val userService: UserService
) {

    @PostMapping
    fun createUser(@Valid @RequestBody userDto: UserDto): ResponseEntity<UserDto> {
        val newUser = userService.create(userDto)
        val uri = URI.create("/users/${newUser.id}")
        return ResponseEntity.created(uri).body(newUser)
    }

    @GetMapping
    fun listUsers() = ResponseEntity.ok(userService.getAll())

    @GetMapping("/{id}")
    fun getUser(@PathVariable id:UUID) = ResponseEntity.ok(userService.findOne(id))

    @PutMapping("/{id}")
    fun updateUser(@PathVariable id: UUID, @RequestBody user: UpdateUserDto) = userService.update(id, user)

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable id:UUID): ResponseEntity<HttpStatus> {
        userService.delete(id)
        return ResponseEntity.noContent().build()
    }
}