package com.kotlinblog.kotlinblog.service

import com.kotlinblog.kotlinblog.dto.UpdateUserDto
import com.kotlinblog.kotlinblog.dto.UserDto
import com.kotlinblog.kotlinblog.exception.UserNotFoundException
import com.kotlinblog.kotlinblog.model.User
import com.kotlinblog.kotlinblog.repository.UserRepository
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class UserService(
    val userRepository: UserRepository
) {
    fun create(userDto: UserDto): UserDto {
        return userRepository.save(userDto.toUser()).toUserDto()
    }

    fun getAll() = userRepository.findAll().map { it.toUserDto() }

    fun findOne(id: UUID): UserDto {
        val user = userRepository.findById(id).orElseThrow { UserNotFoundException() }
        return user.toUserDto()
    }

    fun update(id: UUID, updateUser: UpdateUserDto): UserDto {
        val user = userRepository.findById(id).orElseThrow { UserNotFoundException() }
        return userRepository.save(
            User(
                id = user.id,
                name = updateUser.name?:user.name,
                email = updateUser.email?:user.email,
                password = updateUser.password?:user.password
            )
        ).toUserDto()
    }

    fun delete(id: UUID) {
        if (!userRepository.existsById(id)) throw UserNotFoundException()
        userRepository.deleteById(id)
    }

}
